/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CONF_H_
#define CONF_H_

#include "ax5043.h"

/**
 * @file conf.h
 * General configuration file
 */
#define CALLSIGN_STATION                (uint8_t*) "SV0ZXY"
#define CALLSIGN_DESTINATION            (uint8_t*) "SV"

#define APRS_UHF                        432500000

/******************************************************************************
 ************************* RF Configuration ***********************************
 *****************************************************************************/
#define RX_FREQ_HZ                      APRS_UHF
#define TX_FREQ_HZ                      APRS_UHF
//#define APRS_MSG_FORMAT_LONG

/* Reference Oscillator frequency */
#if PQ9ISH_DEV_BOARD
#define XTAL_FREQ_HZ                    48000000
#else
#define XTAL_FREQ_HZ                    26000000
#endif

/**
 * External PA Control
 */

#define AX5043_EXT_PA_ENABLE            0
#define AX5043_EXT_PA_DISABLE           1

/**
 * Ramp up/Ramp down period of the power amplifier in microseconds
 */
#define PWRAMP_RAMP_PERIOD_US           200

#define AX5043_RF_SWITCH_ENABLE         ANTSEL_OUTPUT_1
#define AX5043_RF_SWITCH_DISABLE        ANTSEL_OUTPUT_0

/**
 * Enables/Disables the appropriate setup for deployment on the devboards or
 * the
 */
#define PQ9ISH_DEV_BOARD 0

#endif /* CONF_H_ */
