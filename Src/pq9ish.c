/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017,2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pq9ish.h>
#include "conf.h"
#include "ax5043.h"

static void
disable_pa(ax5043_conf_t *hax)
{
  if(!hax) {
    return;
  }
  ax5043_pwramp_control(hax, AX5043_EXT_PA_DISABLE);
//  ax5043_set_antsel(hax, 0, 0, AX5043_RF_SWITCH_DISABLE);
}

int
pq9ish_rf_init(pq9ish_rf_conf_t *conf, SPI_HandleTypeDef *hspi)
{
  int ret;
  if(!conf || !hspi) {
    return -PQ9ISH_INVALID_PARAM;
  }

  ret = ax5043_init(&conf->hax5043, hspi, XTAL_FREQ_HZ, VCO_INTERNAL,
                    &disable_pa);
  ax5043_pwramp_control(&conf->hax5043, AX5043_EXT_PA_DISABLE);
  ax5043_set_antsel(&conf->hax5043, 0, 0, AX5043_RF_SWITCH_ENABLE);
  if(ret) {
    return ret;
  }

  return PQ9ISH_SUCCESS;
}
/*
int
pq9ish_tx_weather_aprs(pq9ish_rf_conf_t *conf, const weather_data_t *w)
{
  int ret;
  ax5043_set_antsel(&conf->hax5043, 0, 0, AX5043_RF_SWITCH_ENABLE);
  ax5043_enable_pwramp(&conf->hax5043, AX5043_EXT_PA_ENABLE);
  ret = aprs_tx_weather_report(&conf->haprs, &conf->hax5043, w);
  return ret;
}
*/
