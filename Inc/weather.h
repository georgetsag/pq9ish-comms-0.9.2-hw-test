/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WEATHER_H_
#define WEATHER_H_

#include <stdint.h>
#include "atmo.h"

typedef struct
{
  uint16_t pressure;
  int8_t temperature;
  uint8_t humidity;
  uint16_t pressure_altitude;
  uint8_t atmo_internal_temp;
  uint16_t gas_co;
  uint16_t muon_count;
  uint16_t muon_level;
  int8_t external_temp1;
  int8_t external_temp2;
  flight_phase_t flightPhase;
  int16_t verticalVelocity;
  uint8_t system_state;
  uint16_t batt_milivolt;
} weather_data_t;

//typedef struct
//{
//  uint16_t val1;
//  uint16_t val2;
//  uint16_t val3;
//  uint16_t val4;
//} pq_atmo_values_t;

typedef struct {
        uint16_t muon_level;
        uint16_t muon_count;
        uint16_t mcu_temp;
        uint16_t gas_CO;
} pq_atmo_sensor_t;

typedef struct {
        uint16_t pressure_hi_alt;
        uint16_t ext_temp1;
        uint16_t ext_temp2;
        uint16_t batt;
        CAN_message_id sensor_type;
} pq_atmo_sensor2_t;

typedef struct {
        uint16_t pressure;
        uint16_t temperature;
        uint16_t humidity;
        uint16_t pressure_altitude;
} pq_atmo_atmospheric_data_t;

int
weather_update_atmo(weather_data_t *w, pq_atmo_atmospheric_data_t *data);

int
weather_update_muon(weather_data_t *w,pq_atmo_sensor_t *data);

int
atmo_request_handler();
int atmo_update_sensor2(weather_data_t *weather_data, pq_atmo_sensor2_t *RxData);
#endif /* WEATHER_H_ */
